<?php
session_start(); // On démarre la session AVANT toute chose
?>
<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Ludothèque</title>
		<link rel="stylesheet" type="text/css" href="design.css" media="all" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>

    
	<body>
    <div id ="banner">
		<h1>Bienvenue sur le site de la ludothèque.</h1>
		<img src="http://promokot.pl/wp-content/uploads/2016/07/dice-1265633_1280-145x100.jpg" alt="logo" id ="logo">

    
		
		<!-- Bouton "Nos jeux" -->
		<a href="Jeux.php"><button class = button>Nos jeux</button></a>
		<!-- Bouton "Contact" -->
		<a href="Contact.php"><button class = button>Contact</button></a>
		<!-- Bouton "Presentation" -->
		<a href="Equipe.php"><button class = button>Présentation</button></a>
		<!-- Bouton "Login" -->
		  <?php
            if (empty($_SESSION['login'])){
                echo "<a href='Login.php'><button class=login>Se connecter</button></a>";
               
                echo "<a href='signin.php'><button class=login>S'inscrire</button></a>";
            }
            else{
                echo "<p class='login'> Bonjour ".$_SESSION['login']." </p>";
		echo "<a href='deco.php'><button class=login>Se deconnecter</button></a>";
            }
        ?>
	
        <!-- Slider -->
       </div>
       
       <div class = "large_textbox">
          	
		    Horaires d'ouvertures : 
	    	<ul>
    			<li>Lundi : 15h 18h</li>
    			<li>Mardi : 8h 12h 14h 18h</li>
    			<li>Mercredi : 8h 12h 14h 18h</li>
    			<li>Jeudi : 8h 12h 14h 18h</li>
    			<li>Vendredi : 8h 12h 14h 18h </li>
    			<li>Samedi 8h 12h </li>
		    </ul>
          
       </div>
      
       <div class = "large_textbox"> Nos derniers Jeux.</div>
             <section id="slideshow">
 		
             	<div class="container">
             		<div class="c_slider"></div>
             		<div class="slider">
             			<figure>
             				<img src="Images/croquecarotte.jpg" alt="" width="640" height="310" />
             				<figcaption>Votre lapin réussira-t-il à gravir la montagne le premier ?</figcaption>
             
             			</figure><!--
             			--><figure>
             				<img src="Images/tetris.JPG" alt="" width="640" height="310" />
             				<figcaption>Comme sur arcade, mais sans l'arcade</figcaption>
             			</figure><!--
             			--><figure>
             				<img src="Images/onedirection.jpg" alt="" width="640" height="310" />
             				<figcaption>Same Mistakes !</figcaption>
             
             			</figure><!--
             			--><figure>
             				<img src="Images/richesse du monde.jpeg" alt="" width="640" height="310" />
             				<figcaption>Un joli jeu pour toute la famille</figcaption>
             			</figure>
             		</div>
             	</div>
             		
             	<span id="timeline"></span>
             </section>
	

	</body>
</html>
