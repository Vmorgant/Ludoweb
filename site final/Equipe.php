<?php
session_start(); // On démarre la session AVANT toute chose
?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Notre Equipe</title>
  <link rel="stylesheet" type="text/css" href="design.css" media="all" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>


<body>

<div id ="banner">
		<img src="http://promokot.pl/wp-content/uploads/2016/07/dice-1265633_1280-145x100.jpg" alt="logo" id ="logo">
		
		<!-- Bouton "Nos jeux" -->
		<a href="Jeux.php"><button class = button>Nos jeux</button></a>
		<!-- Bouton "Contact" -->
		<a href="Contact.php"><button class = button>Contact</button></a>
		<!-- Bouton "Presentation" -->
		<a href="Equipe.php"><button class = button>Présentation</button></a>
          <?php
            if (empty($_SESSION['login'])){
                echo "<a href='Login.php'><button class=login>Se connecter</button></a>";
               
                echo "<a href='signin.php'><button class=login>S'inscrire</button></a>";
            }
            else{
                echo "<p class='login'> Bonjour ".$_SESSION['login']." </p>";
		echo "<a href='deco.php'><button class=login>Se deconnecter</button></a>";
            }
        ?>
       </div>
<div class="large_textbox">
L'équipe de la ludothèque composée de nombreux expert en jeux est à votre écoute pour toutes questions:
<br />  
<ul>
<li>Martin Lebourdais </li>
<li>Responsable - Ludothécaire.</li>
<li>Ses Jeux Préférés : Croque Carotte, Loup Garou, Tofu Kingdom.</li>
<br /> 
<li>Victor Morgant</li> 
<li>Médiateur culturel par le jeu. </li>
<li>Ses Jeux Préférés : Tetris, Star Wars Armada, Richesse du monde.</li>
<br /> 
<li>John Doe </li>
<li>Agent d'accueil.</li> 
<li>Ses Jeux Préférés : One Direction, Loup Garou, Monopoly.</li>
<br /> 
</ul>
</div>

<div class="large_textbox">
REJOIGNEZ L'EQUIPE DE LUDOWEB
<br /> 
Vous souhaitez faire du bénévolat. Vous vous reconnaissez dans les valeurs de Ludoweb ? 
Vous souhaitez vous engager à nos côtés mais vous ne savez pas par quel biais ? 
Devenir administrateur, participer à l’accueil du public, aider à la vérification des jeux, 
participer à des animations régulières ou ponctuelles lors de nos événements, conseiller dans le choix de jeux, 
créer des déguisements, réparer des jouets, … les moyens d’aider sont multiples 
et le temps de l’investissement dépend de vos envies… 
alors n’attendez plus et contactez-nous.
</div>

       <section id="slideshow">
		
            	<div class="container">
            		<div class="c_slider"></div>
            		<div class="slider">
            			<figure>
            				<img src="Images/croquecarotte.jpg" alt="" width="640" height="310" />
            				<figcaption>Votre lapin réussira-t-il à gravir la montagne le premier ?</figcaption>
            
            			</figure><!--
            			--><figure>
            				<img src="Images/tetris.JPG" alt="" width="640" height="310" />
            				<figcaption>Comme sur arcade, mais sans l'arcade</figcaption>
            			</figure><!--
            			--><figure>
            				<img src="Images/onedirection.jpg" alt="" width="640" height="310" />
            				<figcaption>Same Mistakes !</figcaption>
            
            			</figure><!--
            			--><figure>
            				<img src="Images/richesse du monde.jpeg" alt="" width="640" height="310" />
            				<figcaption>Un joli jeu pour toute la famille</figcaption>
            			</figure>
            		</div>
            	</div>
            		
            	<span id="timeline"></span>
            </section>
			
<div>
<a href="#"><button class = button>Aller en haut de page</button></a>
<a href=index.php><button class = button>Accueil</button> </a>
</div>



</body>
</html>
