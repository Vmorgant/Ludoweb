  <?php
session_start(); // On démarre la session AVANT toute chose
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Contact</title>
    <link rel="stylesheet" type="text/css" href="design.css" media="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div id="banner">
        <h1>Bienvenue sur le site de la ludothèque.</h1>
        <img src="http://promokot.pl/wp-content/uploads/2016/07/dice-1265633_1280-145x100.jpg" alt="logo" id="logo">


        <!-- Bouton "Nos jeux" -->
        <a href="Jeux.php"><button class=button>Nos jeux</button></a>
        <!-- Bouton "Contact" -->
        <a href="Contact.php"><button class=button>Contact</button></a>
        <!-- Bouton "Presentation" -->
        <a href="Equipe.php"><button class=button>Présentation</button></a>
        <?php
            if (empty($_SESSION['login'])){
                echo "<a href='Login.php'><button class=login>Se connecter</button></a>";
               
                echo "<a href='signin.php'><button class=login>S'inscrire</button></a>";
            }
            else{
                echo "<p class='login'> Bonjour ".$_SESSION['login']." </p>";
		$login = $_SESSION['login'];
            }
        ?>
    </div>
	
    <div class="textbox">
        <p align="center" class="textbox"><strong>Contact</strong></p>
        <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
            <form action="envoi.php" method="post" enctype="application/x-www-form-urlencoded" name="formulaire">
                <tr>
                    <td colspan="3"><strong>Envoyer un message</strong></td>
                </tr>
                <tr>
                    <td><div align="left">Votre nom :</div></td>
                   <?php echo "<td colspan='2'><input type='text' name='nom' size='45' maxlength='100'value=".$login." </td>" ?>
                </tr>
                <tr>
                    <td width="17%"><div align="left">Votre mail :</div></td>
                    <td colspan="2"><input type="text" name="mail" size="45" maxlength="100"></td>
                </tr>
                <tr>
                    <td><div align="left">Sujet : </div></td>
                    <td colspan="2"><input type="text" name="objet" size="45" maxlength="120"></td>
                </tr>
                <tr>
                    <td><div align="left">Message : </div></td>
                    <td colspan="2"><textarea name="message" cols="50" rows="10"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td width="42%">
                        <center>
                            <input type="reset" name="Submit" value="Réinitialiser le formulaire">
                        </center>
                    </td>
                    <td width="41%">
                        <center>
                            <input type="submit" name="Submit" value="Envoyer">
                        </center>
                    </td>
                </tr>
            </form>
        </table>
        </div>
		
 <div>
	<a href="#"><button class = button>Aller en haut de page</button></a>
	<a href=index.php><button class = button>Accueil</button> </a>
</div>
</body>
</html>
