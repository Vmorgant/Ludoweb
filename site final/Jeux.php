<?php
session_start(); // On démarre la session AVANT toute chose
?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Nos jeux</title>
  <link rel="stylesheet" type="text/css" href="design.css" media="all" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>


<body>
 <div id ="banner">
		<h1>Bienvenue sur le site de Ludoweb.</h1>
		<img src="http://promokot.pl/wp-content/uploads/2016/07/dice-1265633_1280-145x100.jpg" alt="logo" id ="logo">

    
		
		<!-- Bouton "Nos jeux" -->
		<a href="Jeux.php"><button class = button>Nos jeux</button></a>
		<!-- Bouton "Contact" -->
		<a href="Contact.php"><button class = button>Contact</button></a>
		<!-- Bouton "Presentation" -->
		<a href="Equipe.php"><button class = button>Présentation</button></a>
		  <?php
            if (empty($_SESSION['login'])){
                echo "<a href='Login.php'><button class=login>Se connecter</button></a>";
               
                echo "<a href='signin.php'><button class=login>S'inscrire</button></a>";
            }
            else{
                echo "<p class='login'> Bonjour ".$_SESSION['login']." </p>";
		echo "<a href='deco.php'><button class=login>Se deconnecter</button></a>";
            }
        ?>
        
       </div>

	<div class = "large_textbox">
		Notre ludothèque possède de nombreux jeux que ce soit dans notre collection permanente ou temporaire. 
		</br>Trouvez le jeu qui vous convient grace à notre formulaire en ligne.</div>
	<div class = "large_textbox"> Nos derniers Jeux.</div>
		      <section id="slideshow">
          	<div class="container">
            		<div class="c_slider"></div>
            		<div class="slider">
            			<figure>
            				<img src="Images/croquecarotte.jpg" alt="" width="640" height="310" />
            				<figcaption>Votre lapin réussira-t-il à gravir la montagne le premier ?</figcaption>
            
            			</figure><!--
            			--><figure>
            				<img src="Images/tetris.JPG" alt="" width="640" height="310" />
            				<figcaption>Comme sur arcade, mais sans l'arcade</figcaption>
            			</figure><!--
            			--><figure>
            				<img src="Images/onedirection.jpg" alt="" width="640" height="310" />
            				<figcaption>Same Mistakes !</figcaption>
            
            			</figure><!--
            			--><figure>
            				<img src="Images/richesse du monde.jpeg" alt="" width="640" height="310" />
            				<figcaption>Un joli jeu pour toute la famille</figcaption>
            			</figure>
            		</div>
            	</div>
            		
            	<span id="timeline"></span>
            </section>
		<br/>
		<div class = "large_textbox">Merci d'utiliser la zone de recherche ci dessous pour trouver le jeu qui vous convient.
 	</div>
<div>
<form method="post" action="recherche.php">
	<fieldset>
		<select name="Age">
			<option value="0"> Age </option>
			<option value="1"> Moins de 6 ans</option>
			<option value="6">De 6 à 12 ans</option>
			<option value="12">12 ans et plus</option>
		</select>
		<select name="NombreMin">
			<option value="0"> Nombres de joueurs Minimum</option>
			<option value="1">1 joueurs</option>
			<option value="2">2 joueurs</option>
			<option value="4">4 joueurs et plus </option>
		</select>
		<select name="NombreMax">
			<option value="99"> Nombres de joueurs Maximum</option>
			<option value="2">Jusqu'à 2 joueurs</option>
			<option value="5">Jusqu'à 5 joueurs</option>
		</select>
		
		<select name="Duree">
			<option value="999"> Durée de la partie</option>
			<option value="20"> Moins de 20 minutes </option>
			<option value="45">20 à 45 minutes</option>
		</select>
		<select name="Editeur">
			<option value="'%'"> Editeur</option>
			<option value="'Asmodée'"> Asmodée</option>
			<option value="'Hasbro'">Hasbro</option>
			<option value="'Ravensburger'">Ravensburger</option>
			<option value="'Autre'">Autre</option>
		</select>
		<select name="Type">
			<option value="'%'"> Type de jeux</option>
			<option value="'Int'"> Intérieur</option>
			<option value="'Ext'">Extérieur</option>
			
		</select>
		<select name="Stock">
			<option value="0"> Tous les jeux</option>
			<option value="1"> En stock</option>
			
		</select>
		<input class = buton type="submit" value="Rechercher" name = "Rechercher" />
	</fieldset>
</form>


</div>
<br/>
<br/>
<div>
<a href="#"><button class = button>Aller en haut de page</button></a>
<a href=index.php><button class = button>Accueil</button> </a>
</div>



</body>
</html>